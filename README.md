Our 17 locations are packed with the newest washers, dryers, ovens, fridges, freezers, hood fans, microwaves, propane appliances and more. Our premium service, knowledgeable and friendly staff, and always competitive prices will ensure that you get great value on your new kitchen or home appliances!

Address: 8488 Main St, Vancouver, BC V5X 4W8, Canada

Phone: 604-321-6644

Website: https://www.coastappliances.com/locations/vancouver